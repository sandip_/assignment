# Directory Structure
* `backend` dir contains "Task Management APIs"
* `frontend` dir contains UI for "Surprise Magic Box"
* Postman collection is available at `backend/postman-collection.json`
* Instruction to use backend project is briefed at `backend/README.md`
* Instruction to use frontend project is briefed at `frontend/README.md`

# Deployment Info
* Frontend is deployed [here](https://ng-recipe-book-67757.web.app/)
```
https://ng-recipe-book-67757.web.app/
```
* Backend is deployed [here](https://backend-tupp.onrender.com/)
```
https://backend-tupp.onrender.com/
```


# Backend docker Image
* Dockerfile is available at `backend/Dockerfile` for microservices bases deployment.
