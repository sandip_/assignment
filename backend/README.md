# Task Management APIs
* This backend project contains various tasks management apis like,
    - Create Task
    - Update Task
    - Delete Tasks
    - Get All Tasks with custom pagination
    - Get All Tasks assigned to anyone username with custom pagination
    - Get All Tasks of any specific category with custom pagination
* This backend project also includes tests case to test all apis over CI to deliver error free release.

---
# Steps to create a backend project
### This is one time setup 
* For this prject no need to follow steps since these project is already ready to use.
* Run below command to install basic dependencies
```
npm init -y
npm install typescript --save-dev
npx tsc --init
npm install express @types/express --save
npm install --save-dev ts-node-dev
npm install jest @types/jest ts-jest supertest @types/supertest --save-dev
```
* Modify package.json and tsconfig.json according to your need
---
# Steps to run the backend project
### Run existing project
* Run development server
```
npm run dev
```
* Command to build dist and use it (typically, Can be used on production servers)
```
npm run start 
```
* To Run Tests
```
npm run test
```


# Deployment Info
* Backend is deployed [here](https://backend-tupp.onrender.com/)
```
https://backend-tupp.onrender.com/
```
