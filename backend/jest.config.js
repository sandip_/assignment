module.exports = {
    moduleFileExtensions: [
        'ts',
        'js'
    ],
    transform: {
        "^.+\\.tsx?$": ["ts-jest", {
        }]
    },
    testMatch: [
        '**/tests/**/*.spec.(ts|js)'
    ],
    testEnvironment: 'node'
};