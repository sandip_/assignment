import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import { Task } from '../models/taskModel';

// Login Dummy user
export const login = (req: Request, res: Response) => {
    const username: Task = req.body.username;
    // Add logic to validate user
    // ...
    // Assuming user is valid, generate and send a JWT
    // For demonstration purposes only i have hardcoded the username
    // For real usecase we should return invalid username or password if they are invalid
    const user = { id: 1, username: username || 'admin' };
    // Please make this key is not checked in to source control and coming from environment variable
    // For demonstration purposes only i have hardcoded it 
    jwt.sign(user, process.env.PORT || 'secretKeyComesHere', { expiresIn: '1d' }, (err: any, token: any) => {
        if (err) {
            res.status(500).json({ error: 'Failed to generate token' });
        } else {
            res.json({ token });
        }
    });
};
