import { Request, Response } from 'express';
import { Task } from '../models/taskModel';

// Mock data storage (replace with your data storage mechanism)
const tasks: Task[] = [];

// Create a new task if does not exist
export const createTask = (req: Request, res: Response) => {
    const newTask: Task = req.body;
    const taskId = newTask.id;
    const taskIndex = tasks.findIndex((t) => t.id === taskId);
    if (taskIndex != -1) {
        res.status(404).json({ error: 'Task already Exists' });
    } else {
        tasks.push(newTask);
        res.status(201).json(newTask);
    }
};

// Retrieve a task by ID
export const getTaskById = (req: Request, res: Response) => {
    const taskId = req.params.id;
    const task = tasks.find((t) => t.id === taskId);
    if (!task) {
        res.status(404).json({ error: 'Task not found' });
    } else {
        res.status(200).json(task);
    }
};

// Update a specific task by ID
export const updateTask = (req: Request, res: Response) => {
    const taskId = req.params.id;
    const updatedTask: Task = req.body;
    const taskIndex = tasks.findIndex((t) => t.id === taskId);
    if (taskIndex === -1) {
        res.status(404).json({ error: 'Task not found' });
    } else {
        tasks[taskIndex] = updatedTask;
        res.status(200).json(updatedTask);
    }
};

// Delete a specific task by ID
export const deleteTask = (req: Request, res: Response) => {
    const taskId = req.params.id;
    const taskIndex = tasks.findIndex((t) => t.id === taskId);
    if (taskIndex === -1) {
        res.status(404).json({ error: 'Task not found' });
    } else {
        const deletedTask = tasks.splice(taskIndex, 1)[0];
        res.status(204).json(deletedTask);
    }
};

// Retrieve all tasks
// Filter tasks by assignedTo and/or category if query parameters are provided
// Paginate tasks if query parameters are provided or return default page 1 with 10 tasks
export const getAllTasks = (req: Request, res: Response) => {
    const assignedTo = req.query.assignedTo as string;
    const category = req.query.category as string;
    const page = parseInt(req.query.page as string) || 1; // Current page (default: 1)
    const pageSize = parseInt(req.query.pageSize as string) || 2; // Number of tasks per page (default: 10)

    let filteredTasks = tasks;

    // Filter tasks based on both assignedTo and category if both query parameters are provided
    if (assignedTo && category) {
        filteredTasks = filteredTasks.filter((task) => task.assignedTo === assignedTo && task.category === category);
    }
    // Filter tasks by assignedTo if only assignedTo is provided
    else if (assignedTo) {
        filteredTasks = filteredTasks.filter((task) => task.assignedTo === assignedTo);
    }
    // Filter tasks by category if only category is provided
    else if (category) {
        filteredTasks = filteredTasks.filter((task) => task.category === category);
    }

    // Calculate pagination values
    const totalTasks = filteredTasks.length;
    const totalPages = Math.ceil(totalTasks / pageSize);

    // Ensure page is within valid bounds
    if (page < 1) {
        res.status(400).json({ error: 'Invalid page number' });
        return;
    }
    if (page > totalPages) {
        res.status(400).json({ error: 'Page number exceeds total pages' });
        return;
    }

    // Calculate the start and end indices for the current page
    const startIndex = (page - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize, totalTasks);

    // Extract tasks for the current page
    const pageTasks = filteredTasks.slice(startIndex, endIndex);

    res.json({
        tasks: pageTasks,
        currentPage: page,
        totalPages: totalPages,
        totalTasks: totalTasks,
    });

};




