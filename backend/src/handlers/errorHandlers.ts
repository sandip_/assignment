import { Request, Response, NextFunction } from 'express';
// import CustomError from './custom-error'; ## import custom error class

function errorHandler(
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    if (process.env.NODE_ENV === 'development') {
        console.error(err.stack); // Log the error for debugging
    }

    // Handle specific types of errors and send appropriate responses
    // if (err instanceof CustomError) {
    //     return res.status(err.statusCode).json({ error: err.message });
    // }
    // If custom error is not available return generic error message
    // Handle other unhandled errors
    return res.status(500).json({ error: 'Internal server error' });
}

export default errorHandler;
