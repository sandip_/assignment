import express from 'express';
import taskRoutes from './routes/taskRoutes';
import authRoutes from './routes/authRoutes';
import { setResponseContentType } from './middlewares/responseContentTypeMiddleware';
import { authenticateJwt } from './middlewares/authMiddleware';
import errorHandler from './handlers/errorHandlers'

const app = express();


// Middleware and other configurations here
app.use(express.json()); // Parse JSON bodies


// Error handler
app.use(errorHandler);

// Include the task routes
app.use('/api/v1', authenticateJwt, setResponseContentType, taskRoutes); // Use '/api/v1' as the base path for task routes
// Include the auth routes
app.use('/auth/v1', setResponseContentType, authRoutes); // Use '/auth/v1' as the base path for auth routes


// Default default route
app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.status(200).send('Task Management service is running!');
});


// Prepare the server
const port = process.env.PORT || 3000;
export const get = () => app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

// Start the server
if (process.env.NODE_ENV !== 'test') {
    get();
}