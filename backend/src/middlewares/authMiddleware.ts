import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    // Please make this key is not checked in to source control and coming from environment variable
    // For demonstration purposes only i have hardcoded it 
    secretOrKey: process.env.PORT || 'secretKeyComesHere',
};

passport.use(
    new JwtStrategy(opts, async (jwtPayload, done) => {
        try {
            // Find the user by ID or any other unique identifier from your database
            // => const user = await User.findById(jwtPayload.id);
            // For demonstration purposes only i have hardcoded the username/id
            // For real usecase we should return use appropriate error message if user is not found
            const user = { id: 1, username: 'admin' };
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        } catch (error) {
            return done(error, false);
        }
    })
);

export const authenticateJwt = passport.authenticate('jwt', { session: false });