// responseContentTypeMiddleware.ts
import { Request, Response, NextFunction } from 'express';

export const setResponseContentType = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    // Set the response content type to JSON
    res.setHeader('Content-Type', 'application/json');
    next();
};
