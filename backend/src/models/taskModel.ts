// Define the Task model
export interface Task {
    id: string;
    title: string;
    description?: string;
    dueDate?: string;
    assignedTo?: string;
    category?: string;
    status: 'Pending' | 'Completed';
}
