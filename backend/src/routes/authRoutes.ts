import express from 'express';
import { login } from '../controllers/authController';

const router = express.Router();

// Define Auth API routes
router.post('/login', login);
router.post('/signup', login); // For demonstration purposes only
router.put('/forgot-password', login); // For demonstration purposes only

export default router;