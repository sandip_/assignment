import express from 'express';
import { createTask, getTaskById, updateTask, deleteTask, getAllTasks } from '../controllers/taskController';
import { login } from '../controllers/authController';

const router = express.Router();

// Define Task API routes
router.post('/task', createTask);
router.get('/task/:id', getTaskById);
router.put('/task/:id', updateTask);
router.delete('/task/:id', deleteTask);
router.get('/tasks', getAllTasks);

export default router;