import agent = require("supertest");
import { get } from '../main';

export const server = get();
// For demonstration purposes, i have added bearer token on header.

export const request = agent(server);

