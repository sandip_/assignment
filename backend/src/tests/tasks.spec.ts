import { request, server } from "./helpers"
import { Task } from '../models/taskModel';

describe('Task Management APIs', () => {
  let jwtToken: string; // Store the JWT token for later tests
  let taskId: string; // Store the task ID for later tests

  // Sample task object for testing
  const sampleTask: Task = {
    id: '1',
    title: 'Sample Task',
    description: 'This is a sample task.',
    assignedTo: 'John',
    status: "Pending"
  };

  describe('GET /', () => {
    it('should validate server is running', () =>
      request
        .get('/')
        .expect('Content-Type', /text\/plain/)
        .expect(200)
    );
  });

  describe('POST /auth/v1/login', () => {
    it('should login and store JWT token', async () => {
      const res = await request.post('/auth/v1/login');
      expect(res.body).toEqual(expect.objectContaining({ token: expect.any(String) }));
      jwtToken = res.body.token; // Store the JWT token for later tests
    });
  });

  describe('POST /tasks', () => {
    it('should create a new task', async () => {
      const res = await request.post('/api/v1/task').set({ 'Authorization': 'Bearer ' + jwtToken }).send(sampleTask);
      expect(res.status).toBe(201);
      expect(res.body).toEqual(expect.objectContaining({ id: expect.any(String) }));
      taskId = res.body.id; // Store the task ID for later tests
    });
  });

  describe('GET /tasks/:id', () => {
    it('should retrieve a task by ID', async () => {
      const res = await request.get(`/api/v1/task/${taskId}`).set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(200);
      expect(res.body).toEqual(expect.objectContaining({ id: taskId }));
    });

    it('should return 404 if task ID is not found', async () => {
      const res = await request.get('/api/v1/task/invalid-id').set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(404);
    });
  });

  describe(`GET /tasks?assignedTo=${sampleTask.assignedTo}`, () => {
    it('should retrieve tasks assignedTo specific username', async () => {
      const res = await request.get(`/api/v1/tasks?assignedTo=${sampleTask.assignedTo}`).set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(200);
      expect(res.body).toEqual(expect.objectContaining({ tasks: expect.arrayContaining([expect.objectContaining({ assignedTo: sampleTask.assignedTo })]) }));
    });
  });

  describe('GET /tasks?page=1&pageSize=10', () => {
    it('should retrieve tasks with pagination', async () => {
      const res = await request.get(`/api/v1/tasks?page=1&pageSize=10`).set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(200);
      expect(res.body).toEqual(expect.objectContaining({ tasks: expect.arrayContaining([expect.objectContaining({ id: sampleTask.id })]) }));
    });
  });

  describe('PUT /tasks/:id', () => {
    it('should update a task by ID', async () => {
      const updatedTask: Task = {
        id: taskId,
        title: 'Updated Task',
        description: 'This is the updated task.',
        status: "Pending"
      };

      const res = await request.put(`/api/v1/task/${taskId}`).set({ 'Authorization': 'Bearer ' + jwtToken }).send(updatedTask);
      expect(res.status).toBe(200);
      expect(res.body.title).toBe('Updated Task');
    });

    it('should return 404 if task ID is not found', async () => {
      const res = await request.put('/api/v1/task/invalid-id').set({ 'Authorization': 'Bearer ' + jwtToken }).send(sampleTask);
      expect(res.status).toBe(404);
    });
  });

  describe('DELETE /tasks/:id', () => {
    it('should delete a task by ID', async () => {
      const res = await request.delete(`/api/v1/task/${taskId}`).set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(204);
    });

    it('should return 404 if task ID is not found', async () => {
      const res = await request.delete('/api/v1/task/invalid-id').set({ 'Authorization': 'Bearer ' + jwtToken });
      expect(res.status).toBe(404);
    });
  });

  // Add more test cases for other APIs (e.g., GET /tasks, GET /tasks?assignedTo=, GET /tasks?category=) as needed


  afterAll(async () => {
    await server.close();
  });
});
