# Surprise Magic Box
* This frontend project contains two modules
    - Home Page
    - Subscriptions
* This also contains two components
    - Header
    - Footer

---
# Steps to create a frontend project
### This is one time setup 
* For this prject no need to follow steps since these project is already ready to use.
* Run below command to install basic dependencies
```
ionic start
```
* start command takes us through guide to choose from various options and does `npm install` at the end to get ready to use frontend project.
---
# Steps to run the frontend project
### Run existing project
```
ionic server (or ionic s)
```
* Command to build 
```
ionic build
```
* Ionic project can be use with capacitot to generate android and ios platform apps.
* To enable capacitor we can run
```
ionic integrations enable capacitor
``` 


# Deployment Info
* Frontend is deployed [here](https://ng-recipe-book-67757.web.app/)
```
https://ng-recipe-book-67757.web.app/
```
