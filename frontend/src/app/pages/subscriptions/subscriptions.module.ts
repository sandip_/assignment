import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SubscriptionsPage } from './subscriptions.page';

import { SubscriptionsPageRoutingModule } from './subscriptions-routing.module';
import { SharedModule } from 'src/app/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubscriptionsPageRoutingModule,
    SharedModule,
  ],
  declarations: [SubscriptionsPage]
})
export class SubscriptionsPageModule { }
